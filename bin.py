#!/usr/bin/env python27

import sys
import time
import select

from mpd import (MPDClient, CommandError)
from socket import error as SocketError

#VARIABLES
DBFile = "db"


HOST = 'localhost'
PORT = '6600'
PASSWORD = False
##
CON_ID = {'host':HOST, 'port':PORT}
## 

## Some functions
def mpdConnect(client, con_id):
	try:
		client.connect(**con_id)
	except SocketError:
		return False
	return True

def mpdAuth(client, secret):
	try:
		client.password(secret)
	except CommandError:
		return False
	return True

client = MPDClient()
if not mpdConnect(client, CON_ID):
	sys.stdout.write("not connected")
	sys.exit(1)


file = open(DBFile, "r")
currentSong = client.currentsong()["file"]
timeElapsed = float(client.status()["elapsed"])
for line in file.read().split('\n'):
	songInfo = line.split("$")
	if currentSong==songInfo[0]:
		lyricFile = songInfo[1]
	else:
		sys.stdout.write("Song not found")
		exit()
cursor = 0
with open(lyricFile) as f:
	lyricLines = f.read().splitlines()
maxLine = len(lyricLines)
while cursor < maxLine:
	subInfo = lyricLines[cursor].split("$")
	if cursor + 1==maxLine:
		sys.stdout.write(subInfo[1])
		exit()
	subInfoNext = lyricLines[cursor + 1].split("$")
	if float(subInfo[0])<=timeElapsed and timeElapsed<float(subInfoNext[0]):
		sys.stdout.write(subInfo[1])
		exit()
	cursor+=1
sys.stdout.write("Unexpected Error")
exit()